package com.reapercodes.expense_tracker.model;

public enum ExpenseCategory {
    entertainment,
    GROCERIES,
    RESTAURANT,
    UTILITIES,
    MISC
}
